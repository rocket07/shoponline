package com.example.shoponline;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shoponline.adapter.ChartAdapter;
import com.example.shoponline.adapter.CheckoutAdapter;
import com.example.shoponline.helper.FireMessage;
import com.example.shoponline.model.AccountModel;
import com.example.shoponline.model.NorekModel;
import com.example.shoponline.model.NotifTokenModel;
import com.example.shoponline.model.OrderModel;
import com.example.shoponline.model.ProductModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONException;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class CheckoutActivity extends AppCompatActivity {
    TextView tvAlamat, tvTotal, tvNama, tvNohp;
    ImageView ivBack, ivInfo;
    ArrayList<ProductModel> list = new ArrayList<>();
    RecyclerView rvChart;
    Button btnAddFIle, btnBayar;
    EditText etFilename;
    int Image_Request_Code = 2342;
    Uri FilePathUri;
    ProgressDialog progressDialog;
    StorageReference storageReference;
    DatabaseReference database;
    String Storage_Path = "file/";
    public String nama;
    AlertDialog.Builder dialog;
    Spinner spKurir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        Locale localeID = new Locale("in", "ID");
        final NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        tvAlamat = findViewById(R.id.tvAlamat);
        tvTotal = findViewById(R.id.tvTotal);
        tvNama = findViewById(R.id.tvNama);
        tvNohp = findViewById(R.id.tvNohp);
        ivBack = findViewById(R.id.ivBack);
        btnAddFIle = findViewById(R.id.btnAddFIle);
        etFilename = findViewById(R.id.etFilename);
        btnBayar = findViewById(R.id.btnBayar);
        ivInfo = findViewById(R.id.ivInfo);
        spKurir = findViewById(R.id.spKurir);

        progressDialog = new ProgressDialog(CheckoutActivity.this);

        storageReference = FirebaseStorage.getInstance().getReference();
        database = FirebaseDatabase.getInstance().getReference();

        rvChart = findViewById(R.id.rvChart);
        rvChart.setHasFixedSize(true);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        if (user != null){
            database.child("account").child(user.getUid()).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    AccountModel accountModel = dataSnapshot.getValue(AccountModel.class);
                    tvNama.setText(accountModel.getNama());
                    tvNohp.setText(accountModel.getNo_hp());
                    tvAlamat.setText(accountModel.getAlamat());
                    nama = accountModel.getNama();
                    tvTotal.setText(formatRupiah.format(Integer.parseInt(getIntent().getExtras().getString("total"))));

                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    DatabaseReference database = FirebaseDatabase.getInstance().getReference();
                    database.child("chart").child(user.getUid()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            list.clear();
                            for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()){
                                ProductModel productModel = noteDataSnapshot.getValue(ProductModel.class);
                                productModel.setKey(noteDataSnapshot.getKey());
                                list.add(productModel);
                            }
                            showRecyclerList();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            System.out.println(databaseError.getDetails()+" "+databaseError.getMessage());
                        }
                    });
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        btnAddFIle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();

                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Pilih bukti pembayaran"), Image_Request_Code);
            }
        });


        btnBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(etFilename.getText().toString())) {
                    Toast.makeText(CheckoutActivity.this, "Upload bukti transfer", Toast.LENGTH_SHORT).show();
                } else {
                    dialog = new AlertDialog.Builder(CheckoutActivity.this);
                    dialog.setCancelable(true);
                    dialog.setTitle("Bayar produk");
                    dialog.setMessage("Yakin data sudah benar?");
                    dialog.setPositiveButton("BAYAR", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            UploadData();
                        }
                    });
                    dialog.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            }
        });

        ivInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase getDatabase = FirebaseDatabase.getInstance();
                DatabaseReference database = getDatabase.getReference();
                database.child("norek").child("-M9s44EdXGZ0cV7eLICy").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        NorekModel norekModel = dataSnapshot.getValue(NorekModel.class);
                        AlertDialog.Builder dialog = new AlertDialog.Builder(CheckoutActivity.this);
                        dialog.setCancelable(true);
                        dialog.setTitle("Rekening Toko" );
                        dialog.setMessage("No Rekening Kami:\nBank BCA\n"+norekModel.getNorek());
                        dialog.setNegativeButton("TUTUP", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }


    private boolean isEmpty(String s){
        return TextUtils.isEmpty(s);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Image_Request_Code && resultCode == RESULT_OK && data != null && data.getData() != null) {

            FilePathUri = data.getData();

            Cursor cursor = getContentResolver().query(FilePathUri, null, null, null, null);
            int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            cursor.moveToFirst();
            etFilename.setText(cursor.getString(nameIndex));
            btnAddFIle.setText("Foto dipilih");
        }
    }


    // Creating Method to get the selected image file Extension from File Path URI.
    public String GetFileExtension(Uri uri) {

        ContentResolver contentResolver = getContentResolver();

        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();

        // Returning the file Extension.
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri)) ;

    }

    public void UploadData() {
        if (FilePathUri != null){

            progressDialog.setTitle("Mohon tunggu...");
            progressDialog.show();

            StorageReference storageReference2nd = storageReference.child(Storage_Path + System.currentTimeMillis() + "." + GetFileExtension(FilePathUri));
            storageReference2nd.putFile(FilePathUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Task<Uri> firebaseUri = taskSnapshot.getStorage().getDownloadUrl();
                    firebaseUri.addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(final Uri uri) {
                            final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String currentDateandTime = sdf.format(new Date());
                            final OrderModel orderModel = new OrderModel(nama, tvAlamat.getText().toString(), uri.toString(), tvNohp.getText().toString(),getIntent().getExtras().getString("total"),currentDateandTime, user.getUid(),"Process", spKurir.getSelectedItem().toString(), "-");
                            database = FirebaseDatabase.getInstance().getReference();
                            final String key = database.child("order").child(user.getUid()).push().getKey();
                            database.child("order").child(key).setValue(orderModel).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    progressDialog.dismiss();

                                    database.child("chart").child(user.getUid()).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                            for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                                                ProductModel productModel = noteDataSnapshot.getValue(ProductModel.class);
                                                OrderModel orderModel1 = new OrderModel(productModel.getNama(), productModel.getGambar(), productModel.getHarga(), key);
                                                database.child("order_detail").push().setValue(orderModel1);
                                            }
                                            database.child("chart").child(user.getUid()).removeValue();
                                            startActivity(new Intent(CheckoutActivity.this, MainActivity.class));
                                            finish();
                                            System.exit(0);

                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                    Toast.makeText(CheckoutActivity.this, "Order berhasil dibuat", Toast.LENGTH_SHORT).show();
                                    FirebaseDatabase getDatabase = FirebaseDatabase.getInstance();
                                    DatabaseReference getRefenence = getDatabase.getReference();
                                    getRefenence.child("token_user").orderByChild("type").equalTo("admin").addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            String title = "Pesanan baru dari "+user.getDisplayName();
                                            String detail = "Segera konfirmasi dan proses pesanan";
                                            for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()){
                                                NotifTokenModel notif = noteDataSnapshot.getValue(NotifTokenModel.class);
                                                try {
                                                    FireMessage f = new FireMessage(title, detail);
                                                    String fireBaseToken=notif.getToken();
                                                    f.sendToToken(fireBaseToken);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }
                            });
                        }
                    })
                            // If something goes wrong .
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {

                                    // Hiding the progressDialog.
                                    progressDialog.dismiss();

                                    // Showing exception erro message.
                                    Toast.makeText(CheckoutActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            });
                }
            });
        }
    }

    private void CheckoutDetail(final String key){
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        database.child("chart").child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    ProductModel productModel = noteDataSnapshot.getValue(ProductModel.class);
                    OrderModel orderModel1 = new OrderModel(productModel.getNama(), productModel.getGambar(), productModel.getHarga(), key);
                    database.child("order_detail").push().setValue(orderModel1);
                }
                database.child("chart").child(user.getUid()).removeValue();
                FilePathUri = null;

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void showRecyclerList(){
        rvChart.setLayoutManager(new LinearLayoutManager(CheckoutActivity.this));
        CheckoutAdapter chartAdapter = new CheckoutAdapter(list);
        rvChart.setAdapter(chartAdapter);
    }
}
