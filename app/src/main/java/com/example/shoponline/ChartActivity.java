package com.example.shoponline;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shoponline.adapter.ChartAdapter;
import com.example.shoponline.model.ProductModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class ChartActivity extends AppCompatActivity {

    DatabaseReference database;
    RecyclerView rvChart;
    ArrayList<ProductModel> list = new ArrayList<>();
    ImageView ivBack;
    Button btnCheckout;
    Integer total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        ivBack = findViewById(R.id.ivBack);
        btnCheckout = findViewById(R.id.btnCheckout);
        rvChart = findViewById(R.id.rvChart);
        rvChart.setHasFixedSize(true);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        database = FirebaseDatabase.getInstance().getReference();
        database.child("chart").child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                total = 0;
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()){
                    ProductModel productModel = noteDataSnapshot.getValue(ProductModel.class);
                    productModel.setKey(noteDataSnapshot.getKey());
                    total = total + Integer.parseInt(productModel.getHarga());
                    list.add(productModel);
                }
                Locale localeID = new Locale("in", "ID");
                NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
                TextView tvTotal = findViewById(R.id.tvTotal);
                if (total.equals(0)){
                    btnCheckout.setBackgroundColor(getResources().getColor(R.color.colorGray));
                    btnCheckout.setClickable(false);
                }
                tvTotal.setText(formatRupiah.format(total));
                showRecyclerList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println(databaseError.getDetails()+" "+databaseError.getMessage());
            }
        });

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(ChartActivity.this, total.toString(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ChartActivity.this, CheckoutActivity.class);
                intent.putExtra("total", total.toString());
                ChartActivity.this.startActivity(intent);
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }

    private void showRecyclerList(){
        rvChart.setLayoutManager(new LinearLayoutManager(ChartActivity.this));
        ChartAdapter chartAdapter = new ChartAdapter(list);
        rvChart.setAdapter(chartAdapter);
    }
}