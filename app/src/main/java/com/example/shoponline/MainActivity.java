package com.example.shoponline;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.shoponline.adapter.ProductAdapter;
import com.example.shoponline.model.AccountModel;
import com.example.shoponline.model.NotifTokenModel;
import com.example.shoponline.model.ProductModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ImageView ivLogout,ivChart,ivAdd, ivListorder, ivProfile;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    FirebaseAuth auth;
    FirebaseAuth.AuthStateListener listener;
    DatabaseReference database;
    String getType;
    RecyclerView rvItem;
    ArrayList<ProductModel> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ivLogout = findViewById(R.id.ivLogout);
        ivChart = findViewById(R.id.ivChart);
        ivAdd = findViewById(R.id.ivAdd);
        ivProfile = findViewById(R.id.ivProfile);
        ivListorder = findViewById(R.id.ivListorder);
        rvItem = findViewById(R.id.rvItem);
        rvItem.setHasFixedSize(true);

        auth = FirebaseAuth.getInstance();
        listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //Mengecek apakah ada user yang sudah login / belum logout
                final FirebaseUser user = firebaseAuth.getCurrentUser();
                FirebaseDatabase getDatabase = FirebaseDatabase.getInstance();
                final DatabaseReference getRefenence = getDatabase.getReference();
                if(user != null){
                    getRefenence.child("account").child(user.getUid()).addChildEventListener(new ChildEventListener() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            //Mengambil daftar item dari database, setiap kali ada turunannya
                            ivProfile.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                                }
                            });
                            ivListorder.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(MainActivity.this, OrderActivity.class));
                                }
                            });
                            AccountModel accountModel = dataSnapshot.getValue(AccountModel.class);
                            getType = accountModel.getType();
                            if (accountModel.getType().equals("admin")){
                                ivAdd.setVisibility(View.VISIBLE);
                                ivChart.setVisibility(View.GONE);
                                ivAdd.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startActivity(new Intent(MainActivity.this, AddProductActivity.class));
                                    }
                                });
                            }else {
                                ivAdd.setVisibility(View.GONE);
                            }
                                getRefenence.child("token_user").orderByChild("uId").equalTo(user.getUid()).addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                        NotifTokenModel notif = dataSnapshot.getValue(NotifTokenModel.class);
                                        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                                        String uid = notif.getuId();
                                        Log.e("Token", "Token Saya : "+ refreshedToken);
                                        NotifTokenModel notifTokenModel = new NotifTokenModel(refreshedToken, uid, getType);
                                        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
                                        database.child("token_user").child(dataSnapshot.getKey()).setValue(notifTokenModel);
                                    }

                                    @Override
                                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                    }

                                    @Override
                                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            //......
                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                            //......
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                            //.....
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            //Digunakan untuk menangani kejadian Error
                            Log.e("MyListData", "Error: ", databaseError.toException());
                        }
                    });
                    database = FirebaseDatabase.getInstance().getReference();
                    database.child("product").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            list.clear();
                            for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()){
                                ProductModel productModel = noteDataSnapshot.getValue(ProductModel.class);
                                productModel.setKey(noteDataSnapshot.getKey());
                                list.add(productModel);
                            }

                            showRecyclerList();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            System.out.println(databaseError.getDetails()+" "+databaseError.getMessage());
                        }
                    });
                }else {
                    Toast.makeText(MainActivity.this, "Keluar dari aplikasi", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };
        ivChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ChartActivity.class));
            }
        });
        ivLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new AlertDialog.Builder(MainActivity.this);
                inflater = getLayoutInflater();
                dialog.setCancelable(true);
                dialog.setTitle("Logout" );
                dialog.setMessage("Yakin ingin membeli produk?");
                dialog.setPositiveButton("Keluar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        auth.signOut();
                    }
                });
                dialog.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    private void showRecyclerList(){
        rvItem.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        ProductAdapter productAdapter = new ProductAdapter(list);
        rvItem.setAdapter(productAdapter);
    }

    //Menerapkan Listener
    @Override
    protected void onStart() {
        super.onStart();
        auth.addAuthStateListener(listener);
    }

    //Melepaskan Litener
    @Override
    protected void onStop() {
        super.onStop();
        if(listener != null){
            auth.removeAuthStateListener(listener);
        }
    }
}
