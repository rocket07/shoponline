package com.example.shoponline;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.shoponline.model.AccountModel;
import com.example.shoponline.model.NorekModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;

public class ProfileActivity extends AppCompatActivity {
    FirebaseUser user;
    DatabaseReference database;
    StorageReference storageReference;
    EditText etName, etNohp, etAlamat;
    Button btnImage, btnSave;
    ImageView ivUserImg,iv_back, ivInfo;
    int Image_Request_Code = 7;
    String Storage_Path = "file/";
    Uri FilePathUri, photoUrl;
    ProgressDialog progressDialog;
    String getKey, getType;
    AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        btnImage = findViewById(R.id.btnImage);
        ivUserImg = findViewById(R.id.ivUserImg);
        etName = findViewById(R.id.etName);
        etNohp = findViewById(R.id.etNohp);
        etAlamat = findViewById(R.id.etAlamat);
        btnSave = findViewById(R.id.btnSave);
        iv_back = findViewById(R.id.iv_back);
        ivInfo = findViewById(R.id.ivInfo);
        user = FirebaseAuth.getInstance().getCurrentUser();
        database = FirebaseDatabase.getInstance().getReference();

        progressDialog = new ProgressDialog(ProfileActivity.this);
        storageReference = FirebaseStorage.getInstance().getReference();
        FirebaseDatabase getDatabase = FirebaseDatabase.getInstance();
        DatabaseReference getRefenence = getDatabase.getReference();
        if(user != null){
            getRefenence.child("account").child(user.getUid()).addChildEventListener(new ChildEventListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    //Mengambil daftar item dari database, setiap kali ada turunannya
                    AccountModel accountModel = dataSnapshot.getValue(AccountModel.class);
                    etName.setText(user.getDisplayName());
                    etNohp.setText(accountModel.getNo_hp());
                    etAlamat.setText(accountModel.getAlamat());
                    photoUrl = Uri.parse(accountModel.getPict());
                    Glide.with(ProfileActivity.this).load(photoUrl).into(ivUserImg);
                    getKey = dataSnapshot.getKey();
                    getType = accountModel.getType();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    //......
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    //......
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    //.....
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    //Digunakan untuk menangani kejadian Error
                    Log.e("MyListData", "Error: ", databaseError.toException());
                }
            });
        }

        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();

                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Please Select Image"), Image_Request_Code);
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEmpty(etName.getText().toString()) && !isEmpty(etNohp.getText().toString()) && !isEmpty(etAlamat.getText().toString())){
                    progressDialog.setTitle("Update Process...");
                    progressDialog.show();
                    UploadImageFileToFirebaseStorage();
                }else {
                    Snackbar.make(findViewById(R.id.btnSave), "Periksa kembali data anda", Snackbar.LENGTH_LONG).show();
                }
            }
        });

        ivInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase getDatabase = FirebaseDatabase.getInstance();
                DatabaseReference database = getDatabase.getReference();
                database.child("norek").child("-M9s44EdXGZ0cV7eLICy").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        NorekModel norekModel = dataSnapshot.getValue(NorekModel.class);
                        AlertDialog.Builder dialog = new AlertDialog.Builder(ProfileActivity.this);
                        LayoutInflater inflater = getLayoutInflater();
                        dialog.setCancelable(true);
                        dialog.setTitle("Rekening Toko" );
                        final View dialogView = inflater.inflate(R.layout.alert, null);
                        dialog.setView(dialogView);
                        EditText etNorek = dialogView.findViewById(R.id.etNorek);
                        etNorek.setText(norekModel.getNorek());
                        dialog.setPositiveButton("SIMPAN", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                EditText etNorek = dialogView.findViewById(R.id.etNorek);
                                String norek = etNorek.getText().toString();
                                updateNorek(norek);
                                Toast.makeText(ProfileActivity.this, "No Rekening Berhasil diubah", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        });
                        dialog.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

    }

    private boolean isEmpty(String s){
        return TextUtils.isEmpty(s);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Image_Request_Code && resultCode == RESULT_OK && data != null && data.getData() != null) {

            FilePathUri = data.getData();

            try {

                // Getting selected image into Bitmap.
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), FilePathUri);

                // Setting up bitmap selected image into ImageView.
                ivUserImg.setImageBitmap(bitmap);

                // After selecting image change choose button above text.
                btnImage.setText("Image Selected");

            }
            catch (IOException e) {

                e.printStackTrace();
            }
        }
    }


    // Creating Method to get the selected image file Extension from File Path URI.
    public String GetFileExtension(Uri uri) {

        ContentResolver contentResolver = getContentResolver();

        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();

        // Returning the file Extension.
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri)) ;

    }

    public void UploadImageFileToFirebaseStorage() {
        if (FilePathUri != null){

            progressDialog.setTitle("Data is Uploading...");
            progressDialog.show();

            StorageReference storageReference2nd = storageReference.child(Storage_Path + System.currentTimeMillis() + "." + GetFileExtension(FilePathUri));
            storageReference2nd.putFile(FilePathUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Task<Uri> firebaseUri = taskSnapshot.getStorage().getDownloadUrl();
                    firebaseUri.addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(etName.getText().toString())
                                    .setPhotoUri(uri)
                                    .build();
                            user.updateProfile(profileUpdates);
                            AccountModel accountModel = new AccountModel(etAlamat.getText().toString(),etNohp.getText().toString(), getType, uri.toString(), etName.getText().toString());
                            database = FirebaseDatabase.getInstance().getReference();
                            String key = getKey;
                            database.child("account").child(user.getUid()).child(key).setValue(accountModel).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ProfileActivity.this, "Data Berhasil diubah", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    })
                            // If something goes wrong .
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {

                                    // Hiding the progressDialog.
                                    progressDialog.dismiss();

                                    // Showing exception erro message.
                                    Toast.makeText(ProfileActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            });
                }
            });
        }else {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName(etName.getText().toString())
                    .build();
            user.updateProfile(profileUpdates);
            AccountModel accountModel = new AccountModel(etAlamat.getText().toString(),etNohp.getText().toString(), getType, photoUrl.toString(), etName.getText().toString());
            database = FirebaseDatabase.getInstance().getReference();
            String key = getKey;
            database.child("account").child(user.getUid()).child(key).setValue(accountModel).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(ProfileActivity.this, "Data Berhasil diubah", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        }
    }

    private void updateNorek(String norek){
        FirebaseDatabase getDatabase = FirebaseDatabase.getInstance();
        DatabaseReference database = getDatabase.getReference();
        NorekModel norekModel = new NorekModel(norek);
        database.child("norek").child("-M9s44EdXGZ0cV7eLICy").setValue(norekModel);
    }
}
