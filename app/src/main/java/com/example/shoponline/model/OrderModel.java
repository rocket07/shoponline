package com.example.shoponline.model;

public class OrderModel {
    String nama, alamat, bukti_pembayaran,no_hp, total, tgl, nama_barang, gambar_barang, harga_barang, orderKey, key, Uid, status, kurir, resi;

    OrderModel(){}

    public OrderModel(String nama, String alamat, String bukti_pembayaran, String no_hp, String total, String tgl, String uid, String status, String kurir, String resi) {
        this.nama = nama;
        this.alamat = alamat;
        this.bukti_pembayaran = bukti_pembayaran;
        this.no_hp = no_hp;
        this.total = total;
        this.tgl = tgl;
        Uid = uid;
        this.status = status;
        this.kurir = kurir;
        this.resi = resi;
    }

    public OrderModel(String nama_barang, String gambar_barang, String harga_barang, String orderKey) {
        this.nama_barang = nama_barang;
        this.gambar_barang = gambar_barang;
        this.harga_barang = harga_barang;
        this.orderKey = orderKey;
    }

    public String getKurir() {
        return kurir;
    }

    public void setKurir(String kurir) {
        this.kurir = kurir;
    }

    public String getResi() {
        return resi;
    }

    public void setResi(String resi) {
        this.resi = resi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUid() {
        return Uid;
    }

    public void setUid(String uid) {
        Uid = uid;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getBukti_pembayaran() {
        return bukti_pembayaran;
    }

    public void setBukti_pembayaran(String bukti_pembayaran) {
        this.bukti_pembayaran = bukti_pembayaran;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getGambar_barang() {
        return gambar_barang;
    }

    public void setGambar_barang(String gambar_barang) {
        this.gambar_barang = gambar_barang;
    }

    public String getHarga_barang() {
        return harga_barang;
    }

    public void setHarga_barang(String harga_barang) {
        this.harga_barang = harga_barang;
    }

    public String getOrderKey() {
        return orderKey;
    }

    public void setOrderKey(String orderKey) {
        this.orderKey = orderKey;
    }
}
