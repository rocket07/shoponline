package com.example.shoponline.model;

public class ProductModel {
    String nama, harga, deskripsi, gambar, key, qty;

    public ProductModel(String nama, String harga, String deskripsi, String gambar) {
        this.nama = nama;
        this.harga = harga;
        this.deskripsi = deskripsi;
        this.gambar = gambar;
    }

    public ProductModel(String nama, String harga, String gambar, String key, String qty) {
        this.nama = nama;
        this.harga = harga;
        this.gambar = gambar;
        this.key = key;
        this.qty = qty;
    }

    public ProductModel(){}

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
