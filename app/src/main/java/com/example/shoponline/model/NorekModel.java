package com.example.shoponline.model;

public class NorekModel {
    String norek;

    public NorekModel(){}

    public NorekModel(String norek) {
        this.norek = norek;
    }

    public String getNorek() {
        return norek;
    }

    public void setNorek(String norek) {
        this.norek = norek;
    }
}
