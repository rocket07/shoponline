package com.example.shoponline.model;

public class NotifTokenModel {
    String token, key, uId, type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public NotifTokenModel(){}

    public NotifTokenModel(String token, String uId) {
        this.token = token;
        this.uId = uId;
    }

    public NotifTokenModel(String token, String uId, String type) {
        this.token = token;
        this.uId = uId;
        this.type = type;
    }
}
