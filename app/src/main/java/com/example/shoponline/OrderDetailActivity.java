package com.example.shoponline;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shoponline.adapter.CheckoutAdapter;
import com.example.shoponline.adapter.OrderDetailAdapter;
import com.example.shoponline.model.OrderModel;
import com.example.shoponline.model.ProductModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class OrderDetailActivity extends AppCompatActivity {
    ArrayList<OrderModel> list = new ArrayList<>();
    RecyclerView rvChart;
    TextView tvTotal;
    ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        tvTotal = findViewById(R.id.tvTotal);
        ivBack = findViewById(R.id.ivBack);
        String total = formatRupiah.format(Integer.parseInt(getIntent().getExtras().getString("total")));
        tvTotal.setText(total);
        rvChart = findViewById(R.id.rvChart);
        rvChart.setHasFixedSize(true);
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        database.child("order_detail").orderByChild("orderKey").equalTo(getIntent().getExtras().getString("key")).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()){
                    OrderModel productModel = noteDataSnapshot.getValue(OrderModel.class);
                    list.add(productModel);
                }
                showRecyclerList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println(databaseError.getDetails()+" "+databaseError.getMessage());
            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }

    private void showRecyclerList(){
        rvChart.setLayoutManager(new LinearLayoutManager(OrderDetailActivity.this));
        OrderDetailAdapter chartAdapter = new OrderDetailAdapter(list);
        rvChart.setAdapter(chartAdapter);
    }
}