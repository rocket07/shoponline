package com.example.shoponline;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.shoponline.model.AccountModel;
import com.example.shoponline.model.ProductModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.NumberFormat;
import java.util.Locale;

public class DetailProductActivity extends AppCompatActivity {

    ImageView ivBack, ivgambar;
    TextView tvnama, tvharga, tvdetail;
    Button btnAdd;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    String gambar, nama, harga, detail, key;
    View dialogView;
    DatabaseReference database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);

        ivBack = findViewById(R.id.ivBack);
        ivgambar = findViewById(R.id.gambar);
        tvnama = findViewById(R.id.nama);
        tvharga = findViewById(R.id.harga);
        tvdetail = findViewById(R.id.detail);
        btnAdd = findViewById(R.id.btnAdd);

        gambar = getIntent().getExtras().getString("gambar");
        nama = getIntent().getExtras().getString("nama");
        harga = getIntent().getExtras().getString("harga");
        detail = getIntent().getExtras().getString("detail");
        key = getIntent().getExtras().getString("key");

        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        tvnama.setText(nama);
        tvharga.setText(formatRupiah.format(Integer.parseInt(harga)));
        tvdetail.setText(detail);
        Glide.with(DetailProductActivity.this).load(gambar).into(ivgambar);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        database = FirebaseDatabase.getInstance().getReference();
        if (user != null){
            database.child("account").child(user.getUid()).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    AccountModel accountModel = dataSnapshot.getValue(AccountModel.class);
                    if (accountModel.getType().equals("admin")){
                        btnAdd.setText("Hapus");
                        btnAdd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                AlertDialog.Builder dialog = new AlertDialog.Builder(DetailProductActivity.this);
                                dialog.setCancelable(true);
                                dialog.setTitle("Hapus Produk" );
                                dialog.setMessage("Yakin Menghapus Produk ini?");
                                dialog.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        database.child("product").child(key).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Toast.makeText(DetailProductActivity.this, "Produk berhasil dihapus", Toast.LENGTH_SHORT).show();
                                                onBackPressed();
                                                finish();
                                            }
                                        });
                                    }
                                });
                                dialog.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();
                            }
                        });
                    }else {
                        btnAdd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog = new AlertDialog.Builder(DetailProductActivity.this);
                                inflater = getLayoutInflater();
                                dialog.setCancelable(true);
                                dialog.setTitle("Beli produk" );
//                dialogView = inflater.inflate(R.layout.alert, null);
//                dialog.setView(dialogView);
                                dialog.setMessage("Yakin ingin membeli produk?");
                                dialog.setPositiveButton("BELI", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                        EditText etJml = dialogView.findViewById(R.id.etJml);
//                        final String jmlProd = etJml.getText().toString();
                                        buyAction();
                                        //Toast.makeText(DetailProductActivity.this, jmlProd, Toast.LENGTH_SHORT).show();
                                    }
                                });
                                dialog.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();
                            }
                        });
                    }
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });


    }

    private void buyAction(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseDatabase getDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference database = getDatabase.getReference();
        ProductModel productModel = new ProductModel(nama, harga, gambar, key, "1");
        database.child("chart").child(user.getUid()).push().setValue(productModel).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(DetailProductActivity.this, "Berhasil menambahkan produk ke chart!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
