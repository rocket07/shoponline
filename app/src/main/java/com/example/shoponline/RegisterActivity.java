package com.example.shoponline;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shoponline.model.AccountModel;
import com.example.shoponline.model.NotifTokenModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

public class RegisterActivity extends AppCompatActivity {
    TextView tvLogin;
    EditText etEmail, etPass, etName;
    Button btnRegis;
    ProgressDialog progressDialog;
    FirebaseAuth auth;
    String getEmail;
    String getPassword;
    String getName;
    String getAlamat, getNo_hp, getUserId, getType;
    Uri geturlPhoto;
    DatabaseReference database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        tvLogin = findViewById(R.id.tvLogin);
        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        etPass = findViewById(R.id.etPass);
        btnRegis = findViewById(R.id.btnRegis);

        progressDialog = new ProgressDialog(RegisterActivity.this);

        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference();
        etPass.setTransformationMethod(PasswordTransformationMethod.getInstance());

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cekDataUser();
                //Toast.makeText(RegisterActivity.this, spType.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void cekDataUser(){
        //Mendapatkan dat yang diinputkan User
        getEmail = etEmail.getText().toString();
        getPassword = etPass.getText().toString();
        getName = etName.getText().toString();

        //Mengecek apakah email dan sandi kosong atau tidak
        if(TextUtils.isEmpty(getEmail) || TextUtils.isEmpty(getPassword) || TextUtils.isEmpty(getName)){
            Toast.makeText(this, "Nama, Email atau Sandi Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        }else{
            //Mengecek panjang karakter password baru yang akan didaftarkan
            if(getPassword.length() < 6){
                Toast.makeText(this, "Sandi Terlalu Pendek, Minimal 6 Karakter", Toast.LENGTH_SHORT).show();
            }else {
                progressDialog.setTitle("SignUp Process...");
                progressDialog.show();
                createUserAccount();
            }
        }
    }

    private void createUserAccount(){
        auth.createUserWithEmailAndPassword(getEmail, getPassword)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        //Mengecek status keberhasilan saat medaftarkan email dan sandi baru
                        if(task.isSuccessful()){
//                            Toast.makeText(Register.this, "Sign Up Success", Toast.LENGTH_SHORT).show();
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                            getName = etName.getText().toString();
                            geturlPhoto = Uri.parse("https://beautycraftkitchens.com/wp-content/uploads/2017/02/dummy-user.png");
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(getName)
                                    .setPhotoUri(geturlPhoto)
                                    .build();
                            user.updateProfile(profileUpdates);
                            getAlamat = "-";
                            getNo_hp = "-";
                            getType = "user";
                            getUserId = user.getUid();
                            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                            Log.e("Token", "Token Saya : "+ refreshedToken);
                            NotifTokenModel notifTokenModel = new NotifTokenModel(refreshedToken, user.getUid());
                            DatabaseReference database = FirebaseDatabase.getInstance().getReference();
                            database.child("token_user").push().setValue(notifTokenModel);
                            AccountModel accountModel = new AccountModel(getAlamat, getNo_hp, getType, geturlPhoto.toString(), getName);
                            database.child("account").child(getUserId).push().setValue(accountModel);
                            etName.setText("");
                            etEmail.setText("");
                            etPass.setText("");
                            Toast.makeText(RegisterActivity.this, "Register Succsess", Toast.LENGTH_SHORT).show();
                            finish();
                        }else {
                            Toast.makeText(RegisterActivity.this, "Terjadi Kesalahan, Silakan Coba Lagi", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    }
                });
    }
}
