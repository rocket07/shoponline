package com.example.shoponline.adapter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.shoponline.DetailProductActivity;
import com.example.shoponline.R;
import com.example.shoponline.model.ProductModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class ChartAdapter extends RecyclerView.Adapter<ChartAdapter.ListViewHolder> {
    private ArrayList<ProductModel> listProduct;
    DatabaseReference database;
    public ChartAdapter(ArrayList<ProductModel> list){
        this.listProduct = list;
    }
    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chart_item, viewGroup, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, int position) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        final ProductModel product = listProduct.get(position);
        Glide.with(holder.itemView.getContext())
                .load(product.getGambar())
                .apply(new RequestOptions())
                .into(holder.imgPhoto);
        holder.tvNama.setText(product.getNama());
        holder.tvHarga.setText(formatRupiah.format(Integer.parseInt(product.getHarga())));

        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                database = FirebaseDatabase.getInstance().getReference();
                AlertDialog.Builder dialog = new AlertDialog.Builder(holder.itemView.getContext());
                dialog.setTitle("Hapus Produk");
                dialog.setCancelable(true);
                dialog.setMessage("Yakin ingin menghapus produk?");
                dialog.setPositiveButton("HAPUS", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        database.child("chart").child(user.getUid()).child(product.getKey()).removeValue();
                        Toast.makeText(holder.itemView.getContext(),"Produk berhasil dihapus", Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
                dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;
        TextView tvNama, tvHarga, tvDelete;
        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.ivImage);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvHarga = itemView.findViewById(R.id.tvHarga);
            tvDelete = itemView.findViewById(R.id.tvDelete);
        }
    }
}
