package com.example.shoponline.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.shoponline.DetailProductActivity;
import com.example.shoponline.R;
import com.example.shoponline.model.ProductModel;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ListViewHolder> {
    private ArrayList<ProductModel> listProduct;
    public ProductAdapter(ArrayList<ProductModel> list){
        this.listProduct = list;
    }
    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.content_item, viewGroup, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, int position) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        final ProductModel product = listProduct.get(position);
        Glide.with(holder.itemView.getContext())
                .load(product.getGambar())
                .apply(new RequestOptions())
                .into(holder.imgPhoto);
        holder.tvNama.setText(product.getNama());
        holder.tvHarga.setText(formatRupiah.format(Integer.parseInt(product.getHarga())));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), DetailProductActivity.class);
                intent.putExtra("gambar", product.getGambar());
                intent.putExtra("nama", product.getNama());
                intent.putExtra("harga", product.getHarga());
                intent.putExtra("detail", product.getDeskripsi());
                intent.putExtra("key", product.getKey());
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;
        TextView tvNama, tvHarga;
        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.ivImage);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvHarga = itemView.findViewById(R.id.tvHarga);
        }
    }
}
