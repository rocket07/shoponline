package com.example.shoponline.adapter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.shoponline.DetailProductActivity;
import com.example.shoponline.OrderDetailActivity;
import com.example.shoponline.R;
import com.example.shoponline.helper.FireMessage;
import com.example.shoponline.model.AccountModel;
import com.example.shoponline.model.NotifTokenModel;
import com.example.shoponline.model.OrderModel;
import com.example.shoponline.model.ProductModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ListViewHolder> {
    private ArrayList<OrderModel> listProduct;
    DatabaseReference database;
    public OrderAdapter(ArrayList<OrderModel> list){
        this.listProduct = list;
    }
    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.order_item, viewGroup, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, int position) {
        Locale localeID = new Locale("in", "ID");
        final NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        final OrderModel product = listProduct.get(position);
        Glide.with(holder.itemView.getContext())
                .load(product.getBukti_pembayaran())
                .apply(new RequestOptions())
                .into(holder.imgPhoto);
        holder.tvNama.setText(product.getNama());
        holder.tvHarga.setText("Total : "+formatRupiah.format(Integer.parseInt(product.getTotal())));
        holder.tvTgl.setText(product.getTgl());
        holder.tvStatus.setText("Status : "+product.getStatus());
        holder.tvKurir.setText("Kurir : "+product.getKurir());
        holder.tvResi.setText(" |  NoResi : "+product.getResi());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), OrderDetailActivity.class);
                intent.putExtra("key", product.getKey());
                intent.putExtra("total", product.getTotal());
                holder.itemView.getContext().startActivity(intent);
            }
        });

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseDatabase getDatabase = FirebaseDatabase.getInstance();
        DatabaseReference getRefenence = getDatabase.getReference();
        getRefenence.child("account").child(user.getUid()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                AccountModel accountModel = dataSnapshot.getValue(AccountModel.class);
                if (accountModel.getType().equals("admin")){
                    if (product.getStatus().equals("Dikirim")){
                        holder.btnSelesai.setVisibility(View.GONE);
                    }
                    holder.btnSelesai.setTextSize(13);
                    holder.btnSelesai.setText("Konfirmasi & Kirim");
                    holder.btnSelesai.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            database = FirebaseDatabase.getInstance().getReference();
                            AlertDialog.Builder dialog = new AlertDialog.Builder(holder.itemView.getContext());
                            dialog.setTitle("Konfirmasi & Kirim");
                            dialog.setCancelable(true);
                            final View dialogView = LayoutInflater.from(holder.itemView.getContext()).inflate(R.layout.alert, null);
                            dialog.setView(dialogView);
                            EditText etResi = dialogView.findViewById(R.id.etNorek);
                            etResi.setHint("Masukan No Resi");
                            dialog.setCancelable(true);
                            dialog.setPositiveButton("KONFIRMASI", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    EditText etResi = dialogView.findViewById(R.id.etNorek);
                                    String noresi = etResi.getText().toString();
                                    OrderModel orderModel = new OrderModel(product.getNama(), product.getAlamat(), product.getBukti_pembayaran(), product.getNo_hp(),product.getTotal(),product.getTgl(), product.getUid(),"Dikirim", product.getKurir(), noresi);
                                    database.child("order").child(product.getKey()).setValue(orderModel).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            FirebaseDatabase getDatabase = FirebaseDatabase.getInstance();
                                            DatabaseReference getRefenence = getDatabase.getReference();
                                            getRefenence.child("token_user").orderByChild("uId").equalTo(product.getUid()).addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    String title = "Pesanan dikirimkan";
                                                    String detail = "Pesanan anda sudah dikonfirmasi dan dikirim oleh admin";
                                                    for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()){
                                                        NotifTokenModel notif = noteDataSnapshot.getValue(NotifTokenModel.class);
                                                        try {
                                                            FireMessage f = new FireMessage(title, detail);
                                                            String fireBaseToken=notif.getToken();
                                                            f.sendToToken(fireBaseToken);
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });
                                        }
                                    });
                                    Toast.makeText(holder.itemView.getContext(),"Orderan Dikonfirmasi", Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }
                            });
                            dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();
                        }
                    });
                }else {
                    if (product.getStatus().equals("Process")){
                        holder.btnSelesai.setVisibility(View.GONE);
                    }
                    holder.btnSelesai.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            database = FirebaseDatabase.getInstance().getReference();
                            AlertDialog.Builder dialog = new AlertDialog.Builder(holder.itemView.getContext());
                            dialog.setTitle("Selesaikan Orderan");
                            dialog.setCancelable(true);
                            dialog.setMessage("Yakin Proses Order selesai?");
                            dialog.setPositiveButton("SELESAI", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    OrderModel orderModel = new OrderModel(product.getNama(), product.getAlamat(), product.getBukti_pembayaran(), product.getNo_hp(),product.getTotal(),product.getTgl(), product.getUid(),"Selesai", product.getKurir(), product.getResi());
                                    database.child("order").child(product.getKey()).setValue(orderModel);
                                    Toast.makeText(holder.itemView.getContext(),"Orderan Selesai", Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }
                            });
                            dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();
                        }
                    });
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        if (product.getStatus().equals("Selesai")){
            holder.btnSelesai.setVisibility(View.GONE);
        }

        holder.btnDownload.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                PdfDocument document = new PdfDocument();
                // crate a page description
                PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(300, 600, 1).create();
                // start a page
                PdfDocument.Page page = document.startPage(pageInfo);
                Canvas canvas = page.getCanvas();
                Paint paint = new Paint();
                paint.setColor(Color.BLACK);
                canvas.drawText("============== Bukti Transaksi ==============", 5, 10, paint);
                canvas.drawText("Nama : "+product.getNama(), 5, 30, paint);
                canvas.drawText("Alamat : "+product.getAlamat(), 5, 50, paint);
                canvas.drawText("No HP : "+product.getNo_hp(), 5, 70, paint);
                canvas.drawText("TGL : "+product.getTgl(), 5, 90, paint);
                canvas.drawText("Status : "+product.getStatus(), 5, 110, paint);
                canvas.drawText("Kurir : "+product.getKurir(), 5, 130, paint);
                canvas.drawText("Resi : "+product.getResi(), 5, 150, paint);
                canvas.drawText("Total Order : "+formatRupiah.format(Integer.parseInt(product.getTotal())), 5, 170, paint);
                // finish the page
                document.finishPage(page);
// draw text on the graphics object of the page
                // write the document content
                String directory_path = Environment.getExternalStorageDirectory().getPath() + "/ShopOnline Transaksi/";
                File file = new File(directory_path);
                if (!file.exists()) {
                    file.mkdirs();
                }
                String targetPdf = directory_path+product.getNama()+product.getTgl()+".pdf";
                File filePath = new File(targetPdf);
                try {
                    document.writeTo(new FileOutputStream(filePath));
                    Toast.makeText(holder.itemView.getContext(), "Report berhasil dibuat di "+targetPdf, Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    Log.e("main", "error "+e.toString());
                    Toast.makeText(holder.itemView.getContext(), "Something wrong: " + e.toString(),  Toast.LENGTH_LONG).show();
                }
                // close the document
                document.close();
            }
        });

    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;
        TextView tvNama, tvHarga, tvTgl, tvStatus, tvKurir, tvResi;
        Button btnSelesai, btnDownload;
        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.ivImage);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvHarga = itemView.findViewById(R.id.tvHarga);
            btnSelesai = itemView.findViewById(R.id.btnSelesai);
            tvTgl = itemView.findViewById(R.id.tvTgl);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvKurir = itemView.findViewById(R.id.tvKurir);
            tvResi = itemView.findViewById(R.id.tvResi);
            btnDownload = itemView.findViewById(R.id.btnDownload);
        }
    }
}
